<?php

namespace App\Command;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
class NotifyShops extends Command
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected static $defaultName = 'notify:shops';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->entityManager;
        $repo = $em->getRepository("App:Stock");
        $res1 = $repo->findAll();
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers);
        $result = $serializer->normalize($res1, 'json');


        foreach ($result as $stock) {
            $this->notifyShop1($stock);
            $this->notifyShop2($stock);
            $this->notifyShop3($stock);
            $this->notifyShop4($stock);
        }

        $output->writeln('Sent successfully');

        return Command::SUCCESS;
    }

    protected function notifyShop1($stock) {
        exec(
            "curl -X POST -F 'item={$stock['id']}' -F 'stock={$stock['stock']}' https://ksi.nfq.lt/stock.php"
        );
    }

    protected function notifyShop2($stock) {
        exec(
            "curl -X POST -d 'item={$stock['id']}' -d 'stock={$stock['stock']}' https://shop.nfq.lt/stock.php"
        );
    }

    protected function notifyShop3($stock) {
        exec(
            "curl -X POST -H 'Content-Type: application/json' -d '{\"item\": \"{$stock['id']}\", \"stock\": \"{$stock['stock']}\"}' https://shop2.nfq.lt/stock.php"
        );
    }

    protected function notifyShop4($stock) {
        exec(
            "curl -X POST -H 'Content-Type: application/json' -d '{\"item\": \"{$stock['id']}\", \"stock\": \"{$stock['stock']}\"}' https://shop3.nfq.lt/stock.php"
        );
    }
}
