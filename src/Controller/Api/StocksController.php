<?php

namespace App\Controller\Api;

use App\Entity\Stock;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class StocksController extends AbstractController
{

    const RESERVED_DEFAULT = 0;

    public function getStocks($id,SerializerInterface $serializer): JsonResponse
    {
        $stockObject = $this->getDoctrine()
            ->getRepository(Stock::class)
            ->find($id);

        $data = $serializer->normalize($stockObject, 'json');
        return new JsonResponse($data);

    }

    public function postNewStock(Request $request,SerializerInterface $serializer): JsonResponse
    {

        $id=$request->request->get('id');
        $stock=$request->request->get('stock');

        $entityManager = $this->getDoctrine()->getManager();
        $stockObject = new Stock();
        $stockObject->setId($id);
        $stockObject->setStock($stock);
        $stockObject->setReserved(self::RESERVED_DEFAULT);
        $entityManager->persist($stockObject);
        $entityManager->flush();


        $stocksObject = $this->getDoctrine()
            ->getRepository(Stock::class)
            ->findAll();

        $data = $serializer->normalize($stocksObject, 'json');


        return new JsonResponse($data);
    }

    public function makeReservation($id,Request $request,SerializerInterface $serializer): JsonResponse
    {
        $amount=$request->request->get('amount');

        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->createQuery('UPDATE App\Entity\Stock u SET u.reserved = u.reserved + ?1 WHERE u.id = ?2');
        $query->setParameter(1, $amount);
        $query->setParameter(2, $id);
        $result = $query->getResult();
        $entityManager->clear();

        $stockObject = $this->getDoctrine()
            ->getRepository(Stock::class)
            ->find($id);

        $data = $serializer->normalize($stockObject, 'json');
        return new JsonResponse($data);

    }
}
