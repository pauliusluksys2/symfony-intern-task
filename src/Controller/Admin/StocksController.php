<?php

namespace App\Controller\Admin;

use App\Entity\Stock;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class StocksController extends AbstractController
{


    public static $password = 'superSecretPassword';

    public function showAllStocksAction(Request $request): Response
    {

        if ($request->query->get('admin_login')['password'] == self::$password) {
            $stocks = $this->getDoctrine()
                ->getRepository(Stock::class)
                ->findAll();

            return $this->render('list.html.twig', ['stocks' => $stocks]);

        }
        return new RedirectResponse('/admin/login');


    }
}

