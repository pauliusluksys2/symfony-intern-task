<?php

namespace App\Controller\Admin;

use App\Form\AdminLoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{

    public function showLoginForm(): Response
    {
        $form = $this->createForm(AdminLoginType::class);
        return $this->render('login.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
