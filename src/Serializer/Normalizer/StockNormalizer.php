<?php

namespace App\Serializer\Normalizer;

use App\Service\TypeCasting;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class StockNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    private $normalizer;
    private  $typeCasting;
    public function __construct(ObjectNormalizer $normalizer,TypeCasting $typeCasting)
    {
        $this->normalizer = $normalizer;
        $this->typeCasting = $typeCasting;
    }

    public function normalize($object, $format = null, array $context = []): array
    {
        $data = $this->normalizer->normalize($object, $format, $context);

        $i=0;
        $data = [
            "id" => $object->getId(),
            ++$i => $object->getId(),
            "stock" => $object->getStock(),
            ++$i => $object->getStock(),
            "reserved" => $object->getReserved(),
            ++$i => $object->getReserved(),
        ];
        $arrayOnlyStrings=$this->typeCasting->convertIntToString($data);
        return $arrayOnlyStrings;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof \App\Entity\Stock;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
