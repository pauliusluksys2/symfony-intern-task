<?php


namespace App\Service;


class TypeCasting
{
    public function convertIntToString($data)
    {
        foreach ($data as $thisKey => $thisValue) {
            if (is_array($thisValue)) {
                // recurse to handle a nested array
                $data[$thisKey] = $this->convertIntToString($thisValue);
            } elseif (is_integer($thisValue)||is_integer($thisKey)) {
                // convert any integers to a string


                $data[$thisKey] = (string)$thisValue;

            }
        }
        //dd($data);
        return $data;
    }
}