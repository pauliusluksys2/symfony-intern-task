DROP DATABASE IF EXISTS stocks;

CREATE DATABASE stocks;

CREATE TABLE stocks.stocks
(
    id int(11),
    stock int(11),
    reserved int(11)
);

INSERT INTO stocks.stocks (id, stock, reserved) VALUES (123, 3, 1);